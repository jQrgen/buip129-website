const MOCKDATA = {
  elections: {
    '<electionId1>': {
      URI: '<URI>',
      adminId: '<AdminId>',
      contractType: '<contractType>',
      created: '<created>',
      deadline: '<deadline>',
      description: 'description for an election',
      id: '12312451',
      title: 'Some title for an election',
      transactionId: '<transactionId>',
    },
    '<electionId2>': {
      URI: '<URI>',
      adminId: '<AdminId>',
      contractType: '<contractType>',
      created: '<created>',
      deadline: '<deadline>',
      description: 'description for an election',
      id: '12312451',
      title: 'Some title for an election',
      transactionId: '<transactionId>',
    },
    '<electionId3>': {
      URI: '<URI>',
      adminId: '<AdminId>',
      ballot: ['No Vote', 'Yes', 'No'],
      contractType: '<contractType>',
      created: '<created>',
      deadline: '<deadline>',
      description: '[MOCK] description for an election',
      id: '12312451',
      title: '[MOCK] Some title for an election',
      transactionId: '<transactionId>',
    },
  },
  users: {
    '<userId1>': {
      elections: {
        '<electionId>': true,
      },
      fcmKey: '<fcmKeyHere>',
      name: 'BUIP129 user',
    },
  },
  votes: {
    '<electionId3>': {
      '00000000000000000148063ad771cd6631653d02cfc9fdcc9a4839aa9380e12f': 'No',
      'bitcoincash:qqrxa0h9jqnc7v4wmj9ysetsp3y7w9l36u8gnnjulq': 'Yes',
      'bitcoincash:qrcuqadqrzp2uztjl9wn5sthepkg22majyxw4gmv6p': 'No Vote',
    },
  },
};

export default MOCKDATA;
