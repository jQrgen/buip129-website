import { function_url } from '../db';

const humanizeDuration = require('humanize-duration');

const assert = require('assert');

const BLOCK_INTERVAL = 600; // seconds

const blockchainService = {
  async get_tip_height(): Promise<number> {
    const url = function_url('blockchain_tip_height');
    const options = {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
    };

    const response: Response = await fetch(url, options);
    const json = await response.json();
    return Promise.resolve(json.tip_height);
  },

  minutes_to_blocks(minutes: number): number {
    return (minutes * 60) / BLOCK_INTERVAL;
  },

  hours_to_blocks(hours: number): number {
    return this.minutes_to_blocks(hours * 60);
  },

  days_to_blocks(days: number): number {
    return this.hours_to_blocks(days * 24);
  },

  blockHeightToHuman(currentHeight: number, endHeight: number): string {
    // Only supports future time
    assert(endHeight >= currentHeight);
    const seconds = (endHeight - currentHeight) * BLOCK_INTERVAL;
    return `In ${humanizeDuration(seconds * 1000)}`;
  },
};

export default blockchainService;
