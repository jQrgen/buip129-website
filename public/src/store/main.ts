import {
  VuexModule, Module, Mutation, Action,
} from 'vuex-module-decorators';
import { firestore } from '../db';

// NOTE: Vuex Typescript reference implementation
// Ref: https://blog.logrocket.com/how-to-write-a-vue-js-app-completely-in-typescript/
@Module({ namespaced: true })
class Main extends VuexModule {
    private authIdState: string | null = null;

    public db = firestore();

    @Mutation
    public setAuthId(id: string) {
      this.authIdState = id;
    }

    get authId() {
      return this.authIdState;
    }

    @Action
    public signIn(uid: string): void {
      this.context.commit('setAuthId', uid);
    }

    @Action signOut(): void {
      this.context.commit('setAuthId', null);
    }
}

export default Main;
