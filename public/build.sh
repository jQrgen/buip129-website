#!/usr/bin/env bash

# exit when any command fails
set -e

if [ ! -d "./node_modules" ]; then
    npm install
fi

#npm run tsc --build tsconfig.json
#./node_modules/@vue/cli-service/bin/vue-cli-service.js build --modernVy
./node_modules/@vue/cli-service/bin/vue-cli-service.js build
