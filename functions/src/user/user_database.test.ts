import { enableTestMode } from '../db';
import {
  deleteUserAllowedTags, getUserAllowedTags, addUserAllowedTags, deleteUser,
} from './user_database';
import { getMockAddress } from '../mock';
import { hasFirebase } from '../testutil';

enableTestMode();
const skipIf = require('skip-if');

describe('user_database', () => {
  const cleanup = async () => deleteUser(getMockAddress());
  beforeEach(cleanup);
  afterEach(cleanup);

  skipIf(!hasFirebase(), 'user_tags', async () => {
    const user = getMockAddress();
    expect(await getUserAllowedTags(user)).toStrictEqual([]);

    await addUserAllowedTags(user, ['hello', 'big', 'world']);
    expect((await getUserAllowedTags(user)).sort()).toStrictEqual([
      'hello', 'big', 'world',
    ].sort());
    await addUserAllowedTags(user, ['small']);

    await deleteUserAllowedTags(user, ['big', 'small']);
    expect((await getUserAllowedTags(user)).sort()).toStrictEqual(['hello', 'world'].sort());
  });
});
