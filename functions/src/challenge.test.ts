import { IAuth } from './auth/model/auth';
import { generateChallenge, verifyAuthSignature, verifySignature } from './challenge';
import { getMockAddress, getMockChallenge, getMockSignature } from './mock';
import { getRandomInt } from './util';

describe('generateChallenge()', () => {
  it('should not return the same value when invoked twice', () => {
    const invokes = 2;
    let oldChallenge = generateChallenge();
    for (let i = 0; i < invokes; i++) {
      const newChallenge = generateChallenge();
      expect(oldChallenge).not.toBe(newChallenge);
      oldChallenge = newChallenge;
    }
  });

  it('should not return the same when invoked 2-10 times at random', () => {
    const invokes = getRandomInt(1, 10);
    const challenges = [];
    for (let i = 0; i < invokes; i++) {
      const challenge = generateChallenge();
      expect(challenges).not.toContain(challenge);
      challenges.push(challenge);
    }
  });
});

describe('verifySignature', () => {
  it('should return true', () => {
    const mockAuth = {
      challenge: getMockChallenge(),
      address: getMockAddress(),
      signature: getMockSignature(getMockChallenge()),
    } as IAuth;

    expect(verifyAuthSignature(mockAuth)).toBe(true);
    expect(verifySignature(
      getMockChallenge(),
      getMockAddress(),
      getMockSignature(getMockChallenge()),
    )).toBe(true);
  });

  it('should return false', () => {
    const mockAuth = {
      challenge: getMockChallenge(),
      address: getMockAddress(),
      signature: getMockSignature('wrong challenge'),
    } as IAuth;

    expect(verifyAuthSignature(mockAuth)).toBe(false);
    expect(verifySignature(
      getMockChallenge(),
      getMockAddress(),
      getMockSignature('wrong challenge'),
    )).toBe(false);
  });
});
