// eslint-disable-next-line
export const constants = {
  login: {
    path: 'login',
  },
  user: {
    path: 'user',
    fields: {
      allowed_tags: 'allowed_tags',
      link_ringsig_message: 'link_ringsig_message',
      link_ringsig_proof: 'link_ringsig_proof',
      link_ringsig_pubkey: 'link_ringsig_pubkey',
    },
  },
  election: {
    path: 'election',
    fields: {
      adminAddress: 'adminAddress',
      createdAt: 'createdAt',
      participantAddresses: 'participantAddresses',
      tags: 'tags',
      visibility: 'visibility',
    },
  },
};
