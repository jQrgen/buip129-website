import { Blockchain } from 'votepeerjs-hotfix';

/**
 * Returns the block height of the tip of the most-work-chain.
 */
// eslint-disable-next-line import/prefer-default-export
export async function getTipHeight(req: any, res: any): Promise<string> {
  const blockchain = new Blockchain();
  await blockchain.connect();

  const height = await blockchain.getBlockchainTipHeight();

  blockchain.disconnect();

  return JSON.stringify({
    tip_height: height,
  });
}
