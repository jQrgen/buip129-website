import { enableTestMode } from '../../db';
import { getMockPKH } from '../../mock';
import * as Auth from './auth_provider_firebase';
import { hasFirebase } from '../../testutil';

enableTestMode();
const skipIf = require('skip-if');

describe('createJsonWebToken function', () => {
  skipIf(!hasFirebase(), 'should return type string', async () => {
    const jsonWebToken = await Auth.createJsonWebToken(getMockPKH().toString('hex'));
    expect(typeof jsonWebToken).toEqual('string');
  });
  skipIf(!hasFirebase(), 'should not equal an arbitrary string', async () => {
    const jsonWebToken = await Auth.createJsonWebToken(getMockPKH().toString('hex'));

    expect(jsonWebToken).not.toEqual('arbitrary_string');
  });
});
