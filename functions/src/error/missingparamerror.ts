/**
 * Error thrown when a parameter to a API call is missing.
 */
export default class MissingParamError extends Error {
  constructor(param: string) {
    super(`Parameter '${param}' is missing`);
  }
}
